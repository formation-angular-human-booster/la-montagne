import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';

@Component({
  selector: 'app-full-article',
  templateUrl: './full-article.component.html',
  styleUrls: ['./full-article.component.css']
})
export class FullArticleComponent implements OnInit {
  article: Article;
  isLoading: boolean;
  faSpin = faSpinner;
  // Activated route permet de reccupérer les informations
  // de la requête de la page à afficher
  constructor(private route: ActivatedRoute,
              private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    // ne pas oublier l'id à rechercher en paramétre
    this.articleService.getArticleById(+this.route.snapshot.paramMap.get('id')).subscribe((data: Article) =>{
      // quand le serveur répond, j'assigne la réponse à mon attribut article
      this.article = data;
      this.isLoading = false;
    });
  }

  redirectHome() {
    this.router.navigate(['/home']);
  }

}
