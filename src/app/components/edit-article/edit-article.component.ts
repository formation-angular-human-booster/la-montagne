import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {
  articleForm: Article;
  isLoading: boolean;
  constructor(private route: ActivatedRoute,
              private articleService: ArticleService,
              private toastrService: ToastrService,
              private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    // ne pas oublier l'id à rechercher en paramétre
    this.articleService.getArticleById(+this.route.snapshot.paramMap.get('id')).subscribe((data: Article) =>{
      // quand le serveur répond, j'assigne la réponse à mon attribut article
      this.articleForm = data;
      this.isLoading = false;
    });
  }

  onSubmit() {
    this.articleService.edit(this.articleForm);
    this.toastrService.info('Félicitation',
      'Article modifié avec succès', {closeButton: true,
        positionClass: 'toast-bottom-full-width', progressBar: true});
    this.router.navigate(['/admin']);
  }
}
